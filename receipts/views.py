from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin


from receipts.models import Receipt, ExpenseCategory, Account

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/list.html'

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = 'receipts/create.html'
    fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']

    def get_form(self, *args, **kwargs):
        form = super(ReceiptCreateView, self).get_form(*args, **kwargs)
        form.fields['category'].queryset = ExpenseCategory.objects.filter(owner=self.request.user)
        form.fields['account'].queryset = Account.objects.filter(owner=self.request.user)
        return form

    def form_valid(self,form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = 'receipts/categories.html'

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = 'receipts/accounts.html'

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = 'receipts/categorycreate.html'
    fields = ['name']

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect('list_categories')


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = 'receipts/accountcreate.html'
    fields = ['name', 'number']

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect('accounts')
